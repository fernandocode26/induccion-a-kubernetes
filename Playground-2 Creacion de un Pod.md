# Playground 2.0: Creacion de un Pod.

1. Vamos a crear un nginx.
    1. Ejecutamos el comando que mostro la consola al inicio de la creacion del cluster.

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/application/nginx-app.yamlservice/my-nginx-svc
```

2. Listamos los pods creados en los nodos.

```bash
***[node1 ~]$ kubectl get pods***
NAME                        READY   STATUS              RESTARTS   AGE
my-nginx-66b6c48dd5-ks26s   0/1     ContainerCreating   0          15s
my-nginx-66b6c48dd5-xchkm   0/1     ContainerCreating   0          15s
my-nginx-66b6c48dd5-zqmd6   0/1     ContainerCreating   0          15s
```

~~Si queremos mas detalle, ejecutamos este comando:~~

```yaml
***kubectl get pods --all-namespaces -o wide***
```

3. Esperamos unos segundos a espera de los recursos y ejecutamos nuevamente, ya los pods estaran creados.

```bash
***[node1 ~]$ kubectl get pods***
NAME                        READY   STATUS    RESTARTS   AGE
my-nginx-66b6c48dd5-ks26s   1/1     Running   0          35s
my-nginx-66b6c48dd5-xchkm   1/1     Running   0          35s
my-nginx-66b6c48dd5-zqmd6   1/1     Running   0          35s
```

4. Listamos a mayor detalle esos Pods: 

```bash
***kubectl get pods -o wide***
NAME                        READY   STATUS    RESTARTS   AGE   IP         NODE    NOMINATED NODE   READINESS GATES
my-nginx-66b6c48dd5-ks26s   1/1     Running   0          77s   10.5.1.3   node2   <none>           <none>
my-nginx-66b6c48dd5-xchkm   1/1     Running   0          77s   10.5.1.4   node2   <none>           <none>
my-nginx-66b6c48dd5-zqmd6   1/1     Running   0          77s   10.5.1.2   node2   <none>           <none>
```

5. Validamos que funcionen los Pods:
    1. Hacemos una descripcion del pod.
    
    ```yaml
    ***[node1 ~]$ kubectl describe pod my-nginx-66b6c48dd5-ks26s***
    Name:         my-nginx-66b6c48dd5-ks26s
    Namespace:    default
    Priority:     0
    Node:         node2/192.168.0.22
    Start Time:   Mon, 22 Aug 2022 04:25:44 +0000
    Labels:       app=nginx
                  pod-template-hash=66b6c48dd5
    ....
    Events:
      Type    Reason     Age   From               Message
      ----    ------     ----  ----               -------
      Normal  Scheduled  43m   default-scheduler  Successfully assigned default/my-nginx-66b6c48dd5-ks26s to node2
      Normal  Pulling    43m   kubelet            Pulling image "nginx:1.14.2"
      Normal  Pulled     43m   kubelet            Successfully pulled image "nginx:1.14.2" in 14.37902757s
      Normal  Created    42m   kubelet            Created container nginx
      Normal  Started    42m   kubelet            Started container nginx
    ```
    
    1. curl a cada una de las IP mostrada.
        
        ```bash
        ***[node1 ~]$ curl 10.5.1.3***
        <!DOCTYPE html>
        <html>
        <head>
        <title>Welcome to nginx!</title>
        <style>
            body {
                width: 35em;
                margin: 0 auto;
                font-family: Tahoma, Verdana, Arial, sans-serif;
            }
        </style>
        </head>
        <body>
        <h1>Welcome to nginx!</h1>
        <p>If you see this page, the nginx web server is successfully installed and
        working. Further configuration is required.</p>
        
        <p>For online documentation and support please refer to
        <a href="http://nginx.org/">nginx.org</a>.<br/>
        Commercial support is available at
        <a href="http://nginx.com/">nginx.com</a>.</p>
        
        <p><em>Thank you for using nginx.</em></p>
        </body>
        </html>
        ```
        
6. Listamos el deployment desplegado.

```bash
***[node1 ~]$ kubectl get deployments***
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
my-nginx   3/3     3            3           10m
***[node1 ~]$ kubectl get deploy,pod***
NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/my-nginx   3/3     3            3           11m

NAME                            READY   STATUS    RESTARTS   AGE
pod/my-nginx-66b6c48dd5-ks26s   1/1     Running   0          11m
pod/my-nginx-66b6c48dd5-xchkm   1/1     Running   0          11m
pod/my-nginx-66b6c48dd5-zqmd6   1/1     Running   0          11m
```

7. Descargamos la configuracion en nuestra maquina.

```bash
***kubectl get deployment my-nginx -o yaml > podConfiguration.yaml***
```

8. Miramos el file.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"labels":{"app":"nginx"},"name":"my-nginx","namespace":"default"},"spec":{"replica
s":3,"selector":{"matchLabels":{"app":"nginx"}},"template":{"metadata":{"labels":{"app":"nginx"}},"spec":{"containers":[{"image":"nginx:1.14.2","name":"nginx","p
orts":[{"containerPort":80}]}]}}}}
  creationTimestamp: "2022-08-22T04:25:43Z"
  generation: 1
  labels:
    app: nginx
..
    manager: kubectl-client-side-apply
    operation: Update
    time: "2022-08-22T04:25:43Z"
  - apiVersion: apps/v1
    fieldsType: FieldsV1
    fieldsV1:
...
    manager: kube-controller-manager
    operation: Update
    time: "2022-08-22T04:26:19Z"
  name: my-nginx
  namespace: default
  resourceVersion: "5461"
  uid: 21ebe725-44e0-4c28-b619-36e9640b5171
spec:
  progressDeadlineSeconds: 600
  replicas: 3
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: nginx
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx:1.14.2
        imagePullPolicy: IfNotPresent
        name: nginx
        ports:
        - containerPort: 80
          protocol: TCP
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
...
```

9. Ejecutamos el curl en el deployment:

```yaml
***[node1 ~]$ kubectl get svc***
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes     ClusterIP      10.96.0.1       <none>        443/TCP        105m
my-nginx       LoadBalancer   10.97.12.90     <pending>     80:30360/TCP   27m
my-nginx-svc   LoadBalancer   10.100.124.83   <pending>     80:31590/TCP   40m

***[node1 ~]$ curl 10.97.12.90***
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

10. Eliminamos el deployment.

```yaml
**[node1 ~]$ kubectl delete deployment my-nginx**
deployment.apps "my-nginx" deleted
```

11. Validamos:

```yaml
***[node1 ~]$ kubectl get deployments -o wide***
No resources found in default namespace.
```