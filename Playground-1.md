# Playground 1: Creacion del cluster

1. Abrir nuestro buscador en [https://labs.play-with-k8s.com](https://labs.play-with-k8s.com/)
2. Iniciar sesion con la cuenta de GItHub o Docker y presionar Start.
3. Click en + Add new Intance desde el panel de navegacion.
4. Run commands para ver que los componentes pre-instalados se encuentran en el nodo.

```bash
**[node1 ~]$ docker --version**
Docker version 20.10.1, build 831ebea
**[node1 ~]$ kubectl version --output=yaml**  
clientVersion:
  buildDate: "2020-12-18T12:09:25Z"
  compiler: gc
  gitCommit: c4d752765b3bbac2237bf87cf0b1c2e307844666
  gitTreeState: clean
  gitVersion: v1.20.1
  goVersion: go1.15.5
  major: "1"
  minor: "20"
  platform: linux/amd64
```

### Creemos el cluster:

5. Ejecutar el comando kube init para inicializar el cluster.

```bash
kubeadm init --apiserver-advertise-address $(hostname -i) --pod-network-cidr 10.5.0.0/16
```

***Felicidades!, tienes ahora un nodo super nuevo en un cluster de kubernetes.***

```bash
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.0.23:6443 --token 51nurv.clo9dpx8dfgspz3w \
    --discovery-token-ca-cert-hash sha256:8809f136b2fd3013fc4814e69758017336920c1e3779ba42d65d604033910ba0 
Waiting for api server to startup
Warning: resource daemonsets/kube-proxy is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. kubectl apply should only be used on resources created declaratively by either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically.
daemonset.apps/kube-proxy configured
```

Verifiquemos el cluster:

```bash
[node1 ~]$ kubectl get nodes
NAME    STATUS   ROLES                  AGE   VERSION
node1   NotReady    control-plane,master   44m   v1.20.1
```

### Creemos el networking del cluster:

6. Inicialicemos el pod network (cluster networking).
    1. Copiemos el segundo comando de la lista de tres comandos que pueron impresos en pantalla al momento de crear el node1 *(Esto sera un kubectl apply)*

```bash
[node1 ~]$ kubectl apply -f https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter.yaml
configmap/kube-router-cfg created
daemonset.apps/kube-router created
serviceaccount/kube-router created
clusterrole.rbac.authorization.k8s.io/kube-router created
clusterrolebinding.rbac.authorization.k8s.io/kube-router created
```

7. Verificar dentro del cluster el nodo1, halla cambiado de estado a Ready (Esto puede demorar unos segundos).

```bash
[node1 ~]$ kubectl get nodes
NAME    STATUS   ROLES                  AGE   VERSION
node1   Ready    control-plane,master   44m   v1.20.1
```

<aside>
💡 ***Con el Pod network ya inicializado y el control plane Ready, es momento de anadir algunos worker nodes.***

</aside>

### Creemos el nodo worker:

8. Click en + Add new Intance desde el panel de navegacion.
9. Copiemos el largo comando “kubeadm join” que fue mostrado en pantalla como salida del “kubeadm init” en el paso 5.

```bash
kubeadm join 192.168.0.23:6443 --token 51nurv.clo9dpx8dfgspz3w \
    --discovery-token-ca-cert-hash sha256:8809f136b2fd3013fc4814e69758017336920c1e3779ba42d65d604033910ba0 

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
```

10. Validamos los nodos en el nodo principal y debemos tener este output.

```bash
[node1 ~]$ kubectl get nodes
NAME    STATUS   ROLES                  AGE   VERSION
node1   Ready    control-plane,master   53m   v1.20.1
node2   Ready    <none>                 36s   v1.20.1
```

<aside>
💡 **Felicidades, ya tienes cluster de kubernetes tiene dos nodos, un control plane y un nodo de trabajo.**

</aside>